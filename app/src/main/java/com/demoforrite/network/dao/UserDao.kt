package com.demoforrite.network.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.mobinik.motorcare.network.entities.User


@Dao
interface UserDao {

    @Insert
    fun insertUser(user: User)

    @Query("SELECT * FROM user WHERE email= :email AND password= :password")
    fun getUserInfo(email: String, password: String): LiveData<User>
}