package com.mobinik.motorcare.network.repos

import androidx.lifecycle.LiveData
import com.demoforrite.network.AppDatabase
import com.mobinik.motorcare.network.entities.User

class UserRepository(private val appDatabase: AppDatabase) {

    suspend fun insertUser(user: User) = appDatabase.userDao().insertUser(user)

     fun getUserInfo(userEmail: String,password :String) : LiveData<User> = appDatabase.userDao().getUserInfo(userEmail,password)
}