package com.demoforrite

import android.app.Dialog
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.view.View
import android.view.Window
import android.widget.TextView


/**
 * Created by Nikhil on 26/09/21.
 */
object Utils {

    private val TAG = "Utils"

    fun isInternetAvailable(context: Context): Boolean {
        var result = false
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val networkCapabilities = connectivityManager.activeNetwork ?: return false
            val actNw = connectivityManager.getNetworkCapabilities(networkCapabilities) ?: return false
            result = when {
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                else -> false
            }
        } else {
            try {
                connectivityManager.run {
                    connectivityManager.activeNetworkInfo?.run {
                        result = when (type) {
                            ConnectivityManager.TYPE_WIFI -> true
                            ConnectivityManager.TYPE_MOBILE -> true
                            ConnectivityManager.TYPE_ETHERNET -> true
                            else -> false
                        }

                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        return result
    }

    fun Context.showProgressDialog(message: String = "Please Wait"): Dialog {
        val progressDialog = Dialog(this)
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressDialog.setContentView(R.layout.layout_progress_dialog)
        progressDialog.setCanceledOnTouchOutside(false)
        progressDialog.setCancelable(false)
        val mMessage: TextView = progressDialog.findViewById(R.id.progress_message)
        mMessage.text = message
        if (message.isEmpty()) {
            mMessage.visibility = View.GONE
        }
        progressDialog.show()
        return progressDialog
    }

    fun Dialog.hideProgressDialog() {
        if (this.isShowing) this.dismiss()
    }


}
