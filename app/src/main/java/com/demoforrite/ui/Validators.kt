package com.demoforrite.ui


import android.text.TextUtils
import android.util.Log
import com.google.android.material.textfield.TextInputLayout


object Validators {

    fun TextInputLayout.nullCheck(): Boolean {
        if (editText == null) return false
        if (TextUtils.isEmpty(this.editText!!.text)) {
            error = "Field cannot be empty"
            return false
        }
        return true
    }

    fun TextInputLayout.isValidPassword(): Boolean {
        if (editText == null) return false
        if (TextUtils.isEmpty(this.editText!!.text)) {
            error = "Password cannot be empty"
            return false
        }
        if (this.editText!!.text.length < 5) {
            error = "Please create password with more than 5 characters."
            return false
        }
        return true
    }

    fun TextInputLayout.isValidEmail(): Boolean {
        if (editText == null) return false
        if (TextUtils.isEmpty(this.editText!!.text)) {
            error = "Email cannot be empty"
            return false
        }
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(editText!!.text).matches()) {
            error = "Please enter a valid email"
        }
        return true
    }

    fun TextInputLayout.isValidMobileNumber(): Boolean {
        if (editText == null) return false
        if (TextUtils.isEmpty(this.editText!!.text)) {
            error = "Mobile number cannot be empty"
            return false
        }
        return true
    }

    fun TextInputLayout.isValidName(): Boolean {
        if (editText == null) return false
        if (TextUtils.isEmpty(this.editText!!.text)) {
            error = "Name cannot be empty"
            return false
        }
        return true
    }

}
