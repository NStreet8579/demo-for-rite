package com.demoforrite.ui

import com.demoforrite.R
import android.os.Bundle
import android.view.View
import android.view.ViewStub
import android.widget.FrameLayout
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar



public abstract class BaseActivity : AppCompatActivity() {


    private lateinit var mFrameLayout: FrameLayout
    private lateinit var mViewStub: ViewStub


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)



        mFrameLayout = findViewById(R.id.rootView)
        mViewStub = findViewById(R.id.viewStub)

        mViewStub.layoutResource = getLayoutId()
        mViewStub.inflate()
    }


    @LayoutRes
    protected abstract fun getLayoutId(): Int

    fun showSnackBar(view: View, message: String) {
        println("asdasdada asdasdasd asdasdasd")
        Snackbar.make(view, message, Snackbar.LENGTH_SHORT).show()
    }

    fun showInternetSnackBar(view: View) {
        Snackbar.make(view, R.string.internet_error, Snackbar.LENGTH_SHORT).show()
    }
}
