package com.demoforrite.ui.auth

import android.provider.ContactsContract
import androidx.lifecycle.ViewModel
import com.mobinik.motorcare.network.entities.User
import com.mobinik.motorcare.network.repos.UserRepository
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class AuthViewModel(private val repository: UserRepository) : ViewModel() {

    @DelicateCoroutinesApi
    fun insertUser(user: User) = GlobalScope.launch {  repository.insertUser(user)
    }

    fun getUserByEmail(email: String,password :String) = repository.getUserInfo(email,password)
}