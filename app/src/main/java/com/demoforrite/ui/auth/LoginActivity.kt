package com.demoforrite.ui.auth

import com.demoforrite.databinding.ActivityLoginBinding
import com.demoforrite.ui.BaseActivity


import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.demoforrite.R
import com.demoforrite.Utils
import com.demoforrite.ui.Validators.isValidEmail
import com.demoforrite.ui.Validators.isValidPassword
import com.demoforrite.network.AppDatabase
import com.demoforrite.ui.HomeScreen
import com.demoforrite.ui.Validators.nullCheck
import com.mobinik.motorcare.network.repos.UserRepository
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class LoginActivity : BaseActivity(), View.OnClickListener {

    private lateinit var mViewModel: AuthViewModel
    private var mProgressDialog: Dialog? = null
    private lateinit var factory: AuthModelFactory
    private lateinit var db: AppDatabase
    private lateinit var repository: UserRepository


    private val TAG = "LoginActivity"

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, LoginActivity::class.java)
            context.startActivity(intent)
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_login
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val loginBinding: ActivityLoginBinding = DataBindingUtil.setContentView(this, getLayoutId())

        db = AppDatabase(this)
        repository = UserRepository(db)
        factory = AuthModelFactory(repository)

        mViewModel = ViewModelProvider(this, factory).get(AuthViewModel::class.java)
        loginBinding.lifecycleOwner = this
        loginBinding.authModel = mViewModel


        initViews()
    }


    private fun initViews() {
        txtCreateAccount.setOnClickListener(this)
        btnLogin.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.txtCreateAccount -> SignUpActivity.start(this)
            R.id.btnLogin -> initLogin()
        }
    }

    private fun initLogin() {
        if (!Utils.isInternetAvailable(this)) {
            showInternetSnackBar(rootView)
            return
        }
        if (layoutEmail.isValidEmail() && layoutPassword.nullCheck()) {
            CoroutineScope(Dispatchers.Main).launch {
                mViewModel.getUserByEmail(layoutEmail.editText!!.text.toString(),
                    layoutPassword.editText!!.text.toString()).also {
                    it.observe(this@LoginActivity, Observer {
                        if (it?.email != null) {
                            HomeScreen.start(this@LoginActivity, it.name)
                        } else {
                            Toast.makeText(this@LoginActivity, "Invalid Email Or Password", Toast.LENGTH_SHORT)
                                .show()
                        }
                    })
                }
            }
        }
    }

}
