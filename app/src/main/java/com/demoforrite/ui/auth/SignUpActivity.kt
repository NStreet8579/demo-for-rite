package com.demoforrite.ui.auth

import com.demoforrite.R

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.demoforrite.databinding.ActivityLoginBinding
import com.demoforrite.databinding.ActivitySignupBinding
import com.demoforrite.ui.BaseActivity
import com.demoforrite.ui.Validators.isValidEmail
import com.demoforrite.ui.Validators.isValidName
import com.demoforrite.ui.Validators.isValidPassword
import com.demoforrite.network.AppDatabase
import com.mobinik.motorcare.network.entities.User
import com.mobinik.motorcare.network.repos.UserRepository
import kotlinx.android.synthetic.main.activity_signup.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*


class SignUpActivity : BaseActivity(), View.OnClickListener {

    private lateinit var mViewModel: AuthViewModel
    private lateinit var factory: AuthModelFactory
    private lateinit var db: AppDatabase
    private lateinit var repository: UserRepository

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, SignUpActivity::class.java)
            context.startActivity(intent)
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_signup
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val loginBinding: ActivitySignupBinding = DataBindingUtil.setContentView(this, getLayoutId())

        db = AppDatabase(this)
        repository = UserRepository(db)
        factory = AuthModelFactory(repository)

        mViewModel = ViewModelProvider(this, factory).get(AuthViewModel::class.java)
        loginBinding.lifecycleOwner = this
        loginBinding.authModel = mViewModel
        initViews()


    }

    private fun initViews() {
        btnLogin.setOnClickListener(this)
        btnSignUp.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnLogin -> finish()
            R.id.btnSignUp -> initSignUp()
        }
    }

    @DelicateCoroutinesApi
    private fun initSignUp() {
        if (layoutPassword.isValidPassword() && layoutEmail.isValidEmail() && layoutName.isValidName()) {
            val user = User(uId = UUID.randomUUID().toString(), name = layoutName.editText!!.text.toString(),
                email = layoutEmail.editText!!.text.toString(),
                password = layoutPassword.editText!!.text.toString())
            /*CoroutineScope(Dispatchers.Main).launch {

            }*/
            mViewModel.insertUser(user)
            finish()
        }
    }

}
