package com.demoforrite.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.demoforrite.R
import kotlinx.android.synthetic.main.activity_home.*

class HomeScreen : BaseActivity() {

    companion object {
        @JvmStatic
        fun start(context: Context, name: String) {
            val starter = Intent(context, HomeScreen::class.java).putExtra("NAME", name)
            context.startActivity(starter)
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_home
    }


    private var name: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        name = intent.getStringExtra("NAME")
        txtName.text = "Welcome ${name ?: ""}"
    }
}